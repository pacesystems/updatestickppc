#!/bin/sh

# Variables Start
MACHINEARCH=$(uname -m)
# Variables End

# Functions Start
log() {
    if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
        echo -e "$*" >> /dev/tty0
    else
        echo -e "$*"
    fi
}

log "\033[91mYour Stick is not prepared right!\n\033[94mMaybe the Stick is cloned from \033[92mgit\033[94m so you should change the \033[92mbranch\033[94m to be able to run!\033[0m"

#./autoupdate.sh
#./menuupdate.sh
