#!/bin/sh

# Variables Start
MACHINEARCH=$(uname -m)
# Variables End

log() {
    if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
        echo -e "$*" >> /dev/tty0
    else
        echo -e "$*"
    fi
}

if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
    log "# \033[92mUpdating System PPC:\033[0m"
    log "#"
    if [ -e ./update.run ]; then
        chmod +x ./update.run
        ./update.run >> /dev/tty0
        sleep 10s
    else
        log "# \033[91mNo update found!\033[0m"
        log "#"
        sleep 10s
        exit 1
    fi
else
    log "# \033[91mNOT Updating System Files!\033[0m"
    log "#"
    sleep 10s
fi

exit 0
