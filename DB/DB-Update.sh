#!/bin/sh

# Variables Start
MACHINEARCH=$(uname -m)
DESTDIR="/opt/bin"
BACKUPDIR="/media/usb/Backup"
if [ -c /dev/serial-eeprom ]; then          # Determine if /dev/serial-eeprom is a character oriented device so we can reas serial from it
    SERIAL=$(sed -n '/^UNIT_SERIAL:/{s/UNIT_SERIAL://;p}' /dev/serial-eeprom)
else                                        # Otherwise Serial is one
    SERIAL=1
fi
# Variables End

# Functions Start
log() {
    if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
        echo -e "$*" >> /dev/tty0
    else
        echo -e "$*"
    fi
}

printstart() {
    log "Start"
    log ""
    log ""
    sleep 1s
}

printupdate() {
    log "*******************************************************************"
    log "*                                                                 *"
    log "*      ██╗   ██╗██████╗ ██████╗  █████╗ ████████╗███████╗         *"
    log "*      ██║   ██║██╔══██╗██╔══██╗██╔══██╗╚══██╔══╝██╔════╝         *"
    log "*      ██║   ██║██████╔╝██║  ██║███████║   ██║   █████╗           *"
    log "*      ██║   ██║██╔═══╝ ██║  ██║██╔══██║   ██║   ██╔══╝           *"
    log "*      ╚██████╔╝██║     ██████╔╝██║  ██║   ██║   ███████╗         *"
    log "*       ╚═════╝ ╚═╝     ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝         *"
    log "*                                                                 *"
    log "*******************************************************************"
}

printok() {
    log "********************************"
    log "*                              *"
    log "*      ██████╗ ██╗  ██╗        *"
    log "*     ██╔═══██╗██║ ██╔╝        *"
    log "*     ██║   ██║█████╔╝         *"
    log "*     ██║   ██║██╔═██╗         *"
    log "*     ╚██████╔╝██║  ██╗        *"
    log "*      ╚═════╝ ╚═╝  ╚═╝        *"
    log "*                              *"
    log "********************************"
}

printerror() {
    log "***********************************************"
    log "*                                             *"
    log "*  ███████╗██████╗ ██████╗  ██████╗ ██████╗   *"
    log "*  ██╔════╝██╔══██╗██╔══██╗██╔═══██╗██╔══██╗  *"
    log "*  █████╗  ██████╔╝██████╔╝██║   ██║██████╔╝  *"
    log "*  ██╔══╝  ██╔══██╗██╔══██╗██║   ██║██╔══██╗  *"
    log "*  ███████╗██║  ██║██║  ██║╚██████╔╝██║  ██║  *"
    log "*  ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝  *"
    log "*                                             *"
    log "***********************************************"
}
# Functions End

if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
    log "# \033[92mUpdating Database PPC:\033[0m"
    log "#"
    log "' $PWD"
    log "Moving old Database to USB-Stick"
    if [ -d database ]; then
        if [ -e database/main.db ]; then
            if [ ! -d $BACKUPDIR ]; then
                mkdir $BACKUPDIR; 
            fi
            if [ -d $DESTDIR/database ]; then
                cp -r $DESTDIR/database $BACKUPDIR/database_$(date "+%F-%H-%M-%S")
            fi
            log "Moving new Database to PPC"
            cp -r $PWD/database/* $DESTDIR/database
        else
            log "\033[91mNo database for Update found on Stick\033[0m"
            sleep 10s
        fi
    else
        log "\033[91mNo database Folder for Update found on Stick\033[0m"
        sleep 10s
    fi
else
    log "# \033[91mNOT Updating Database\033[0m"
    log "#"
fi

exit 0
