------------------------------------------------------------------------
Version 1.3.0.0
2020-08-20
* Added feature to force rescue mode with button combination.
  - Hold button 2 and 6 on boot to enter rescue mode.
* Added feature to enable or disable USB write caching.
  - Write 1 to the following file to enable: /opt/etc/usb_mount_write_cache 
* Mfgtool now erases /opt partition as well.
* USB-serial driver modules are now loaded in startup procedure.
* CCAux api v2.15.2.0, see separate release notes.

------------------------------------------------------------------------
Version 1.2.8.0
2018-11-16
* Replaced old backgrounds containing Maximatecc logo with new backgrounds containing CrossControl logo

------------------------------------------------------------------------
Version 1.2.7.0
2018-11-14
* removed imx-tests package to fix rootfs problem with mfgtool

------------------------------------------------------------------------
Version 1.2.6.0
2018-10-04
* This version is using thread safe ccapi implementation (CCAux version 2.15.0)
* Bugfix for a multithreading issue affecting the following classes
  - About
  - Auxversion
  - Backlight
  - Buzzer
  - CfgIn
  - Config
  - Diagnostic
  - FrontLED
  - Telematics

------------------------------------------------------------------------
Version 1.2.5.0
2018-09-17
* Starting ccauxd later in boot process seems to be a workaround for the problem.
  - ccapi is not thread safe. Need to fix this problem later.

------------------------------------------------------------------------
Version 1.2.4.0
2018-06-15
* Configure pixelclock drive strength to MED instead of HIGH for units with old display.

------------------------------------------------------------------------
Version 1.2.3.0
2018-06-14
* Fixed CAN driver
  - Added skb_header initialization code to prevent random kernel crashes.

------------------------------------------------------------------------
Version 1.2.2.0
  - Version 1.2.2.1 version uses same source code as version 1.2.2.0
2018-04-25

* Updated Redpine wifi driver to version 1.6.0
  - Hostapd support removed. Wifi AP/STA uses wpa_supplicant.
* Adden iptables, netfilter, nat support to kernel
* Support both new and old display for VC
  - Different PWM backlight frequency (246 Hz) and pixel clock frequency (30 MHz) on new display.
  - In EEPROM, set:
  - DISPLAY_TYPE:1 for old display or
  - DISPLAY_TYPE:2 for new display
  - Also, changed pixel clock drive strength from MED to HIGH.
* Changed backlight PWM frequency from 246 to 246.3 Hz for the new VC-display
* Pixel clock fix:
  - U-boot tried to set pixel clock to 33 MHz, it was set to 29.5 MHz in
  the fb_videomode struct (in ccpilot_xa.c) and default value of pixel
  clock in ldb.c is 28 MHz. Measurement showed that on the currently
  used production image 28 MHz is used. Therefore that is the value
  we will continue to use on the old display (don't change what's working),
  even though I suspect that the intention from the beginning was to
  set something else. The reason why the ldb.c's default value was used
  and not the Kernel command line parameter from U-boot, was a typo in
  the Kernel command line (ld.pixel_clock istead of ldb.pixel_clock).
* Remove dnsmasq from boot
* Added NAT init script. 
  - Allow all connections to mobile network and only existing and related ones in.
  - Default route pointing to mobile network
*  Kernel driver fixes (missing copy_from_user/copy_to_user on user mode parameters):
  - pixcir_i2c: OK
  - ss: OK
  - usb_io: OK
* Fixed ioctl() holding mutex, when there is an error -> DEADLOCK!
* Fixed telematic init scripts
  - telematic_module_power.sh: fixed
  - init-3g: fixed
  - init-wireless: fixed
  - wlan-access-point: fixed
  - wlan-station: fixed
  - ppp: fixed
* Added hotplug support for AI 
  - Load kernel drivers and initialize device when device is connected 
  - Unload kernel drivers when device is disconnected

------------------------------------------------------------------------
Version 1.2.1.0
2017-09-27

* Added CrossLink AI support
* Updated Redpine wifi/bluetooth driver to version 1.5.5
  - New driver adds support to a new revision of the Redpine module
* Added init and helper scripts to control CrossLink AI
* disabled gstreamer tracing to free up disk space
* Using dnsmasq as the default DNS client
* Added PPP default configuration using sonera settings
* PPP configuration is copied to user writable partition on boot if not already there
* Added new libraries / applications
 - libical
 - obexd
 - bluez 4.101
 - python
 - obexftp
 - dnsmasq
 - bluez utils
 - bluez simple agent
* Kernel configuration changes
 - generic usbserial module
 - Bluetooth support
 - packet filtering
 - PPP support
 - wireless support
* Fixed CrossLink AI firmware upgrade
* Fix missing machine-id
* Fix dbus launcher bits
* Increased evdev buffer size to 512

------------------------------------------------------------------------
Version 1.1.0
2015-09-11

* Included ubifs backport patches from git://git.infradead.org/users/dedekind/ubifs-v2.6.35.git
* Fixed issue with suspend failing when CAN messages are received
* Updated CC api to 2.8.3.0
  - Lots of fixes and cleanup from static analysis
  - Support for Config_[set|get]KeySwitchTriggerMode
* Added and updated packages so that analog video works with Qt5 (gstreamer backend)
* Added /dev/ttyUSB[0-7] device nodes and support for FTDI or PL2303 usb/serial converter (as modules)
* Modified system update script (fullup.sh) so that it does not allow dangerous active side updates (can be forced with flag still)
* Support for adding user fonts to system by placing them in /opt/fonts 
* Support for 512M ram variant
* Boot time optimzations (default gui visible < 10 s)
  - Kernel serial console logging disabled by default
  - U-boot silent by default
  - U-boot does only mandatory i2c/EEprom access (display type is known)
  - U-boot does not perform kernel crc check and reads only 3M from nand when it loads kernel
  - Init scripts optimized and StatupLauncher GUI set to start at S04 by default

------------------------------------------------------------------------
Version 1.0.0
2015-01-15

* Updated kernel-headers to devel-package and moved devel-package directory to be under VC-directory to match other platforms.

------------------------------------------------------------------------
Version 0.8.4
2014-12-17

* Fixes the Qt5 startup problems, after non-graceful shutdown of qt5 app
* Added FTDI drivers to main and backup Linux configurations

------------------------------------------------------------------------
Version 0.8.3
2014-12-15

* IPU deadlock fix

------------------------------------------------------------------------
Version 0.8.2
2014-11-24

* Added second USB storage.
* Added invert to QT_QPA_EVDEV_TOUCHSCREEN_PARAMETERS.
* RTC compiled into kernel.
* Use flush instead of sync to speed up USB stick.
* Fix to spurious GPIO buttons.
* Fix to non-working buttons after USB copy.
* ccauxd: Monitors the internal temperature. Gradually lowers the backlight and eventually shuts down the OS when getting close to the maximum operating temperature.

------------------------------------------------------------------------
Version 0.8.1
2014-10-09

* Fix to TVP5150 video power off during suspend.

------------------------------------------------------------------------
Version 0.8.0
2014-10-08

* Keep TVP5150 switched off while not in use.

------------------------------------------------------------------------
Version 0.7.1
2014-09-26

* Test release.
* Invert IPU clock polarity.
* Configuration removed due to move to fb configuration in ccpilot_xa.c.
* Updated default calibration for new display with mirrored y-direction.
* Disable USB H1 and OTG during suspend.
* Set strictModes=no, since we have the authorized keys file in /opt
* Company names changed in scripts.
* Add delay to tslib to make calibration more usable.

------------------------------------------------------------------------
Version 0.7.0
2014-09-24

* Default resolution of VC set in StartupLauncherGui.
* Fixed hanging suspend when touch is pressed during suspend.
* Removed modules not in VC.
* New environment variables and library path for Qt5.
* Removed debug printouts in kernel.

------------------------------------------------------------------------
Version 0.6.1
2014-09-02

* Test release.
* CI minimum frequency threshold changes.

------------------------------------------------------------------------
Version 0.6.0
2014-09-02

* Added code to add slow clock 32kHz output to TP444. Not enabled by default.

------------------------------------------------------------------------
Version 0.5.1
2014-09-02

* CPU register settings for daisy chain and VDD_DIG voltage added.
* Added implementation for IOCTRL_CFGIN_GET_MINFREQTHRESH, IOCTRL_CFGIN_SET_MINFREQTHRESH
* Broken u-boot patches fixed.

------------------------------------------------------------------------
Version 0.5.0
2014-09-01

* firebird links
* Timeout after 1 second when waiting for SPIRDY signal
* Selecting pad EIM_D21 for OTG daisy chain.
* Selecting pad EIM_D30 for H1 daisy chain.
* Setting VDD_DIG_PLL to 1.3V.
* CPU register settings for daisy chain and VDD_DIG voltage added.

------------------------------------------------------------------------
Versio 0.4.1
2014-08-29

* Fix to OC# signal handling. Removed unused code. Changed printk's to pr_debug().
* Fixed OC# handling.

------------------------------------------------------------------------
Version 0.4.0
2014-08-28

* Use NAND to save rescue boot request, bootcount and factory reset bit.
* Fixed wrong result code.
* Use factory-reset device driver.
* Factory reset data area moved to NAND.

------------------------------------------------------------------------
Version 0.3.1
2014-08-26

* Added bootcount increment command incbc to boot arguments.
* Boot count incbc added to u-boot.

------------------------------------------------------------------------
Version 0.3.0
2014-08-22

* fixed check_environment() so that it works on rescue side also
* Touch sensitivity feature added.

------------------------------------------------------------------------
Version 0.2.1
2014-07-31

* Added handlers for H1_OC# and OTG_OC#.
* OC# handlers added.

------------------------------------------------------------------------
Version 0.2.0
2014-07-04

* Set correct timing values for video.

------------------------------------------------------------------------
Version 0.1.5
2014-06-26

* M removed from video definition since it caused CVT parameters to be used and not our video_modes parameters in kernel code.
* Removed extra display modes and unneeded printouts.
* Video mode fix added.

------------------------------------------------------------------------
Version 0.1.4
2014-06-24

* Updated the pm-suspend script with ccauxd FIFO communication.
* Disable biswap during u-boot flashing.
* Add 10 ms sleep after powering tvp_5150 to give it time to wakeup.
* better defaults for display calibration
* Numbering for patches. For easier maintenance.
* Fix video suspend/resume problems by if-0:ing ipu suspend/resume routines.  
* Enable only MP_WAKEUP interrupt to wakeup the system after suspend.

------------------------------------------------------------------------
Version 0.1.3
2014-06-16

* Moved bootcount to iRAM.
* Changed to use EEPROM.
* Fixed optfs destroying for NAND. Use EEPROM factory reset bits.
* Use reboot-rescue.sh to reboot to backup system.
* Changed parameters according to the documentation.
* Added check for nonexistent BOOT variable.
* Changed to point to 2129 device.
* Compile RTC 2129 driver in-built.
* Removed call to prodtest which is nonexistent in bs.

------------------------------------------------------------------------
Version 0.1.2
2014-06-12

* Reverted back due to wrong patch file choice.
* Fix to bootcount writing to EEPROM.
* Removed redundant BOOTCOUNT.
* Reboot-rescue script for VC: Manipulate BOOT variable in EEPROM using BOOT:0 for normal boot and BOOT:5 for backup system boot.

------------------------------------------------------------------------
Version 0.1.1
2014-06-10

* Fix for MP_WAKEUP. Added pads according to HWSW document. SW4AUOMODE=1 and SW4BUOMODE=1 for testing.
* Initial revision with u-boot parameters.
* Added fw_printenv/fw_setenv to system configuration.
* Fixed USB-EN pad. Testing with MP_DONE and MP_RESTART.
* Backlight PWM period dropped from 50us to 40us to avoid noise effects
* add common-io module to build
* minor changes to common-io driver
* Implement IOCTRL_BACKLIGHT_BUTTON_STATUS
* Use ccp_backlight_pwm_period_ns kernel commandline parameter and default to 200 Hz on BL PWM period. On suspend drive 0 to all the NVCC_LCD pins, MP_VTFEN# and CAN_STBY#. On resume restore pad configuration and MP_VTFEN# + CAN_STBY#.
* Added ccp_backlight_pwm_period_ns env variable. Fixed video settings. Added blperiod setting for VC display.
* Changed RGB outputs from LOW to MEDium.
* Do not reset /etc/pointercal on reboot.
* Use EEPROM for boot selection and bootcount.
* Disable VSYNC/HSYNC active high settings.
* Fixed bootcount writing to EEPROM.

------------------------------------------------------------------------
Version 0.1.0
2014-05-19

* Leave socket_can out of the build
* Store defconfig for mfg kernel, requires USB-OTG and FSL-UTP options
* Set OTGID by default, shouldn't affect if OTG is not configured in kernel
* Store config for mfg uboot
* Add required host tools (gen_init_cpio, geninitramfs) and target binaries (uuc)
to generate and use mfg initramfs
* Add target for generating initramfs
* Store ucl for mfgtools
* Update ucl
* Set mfg bootdelay to zero
* Use even looser nand timings
* Use looser nand timings in uboot
* Use 8bit ecc and a bit stricter timings
* Some MTD options missing from bs defconfig
* Update banners
* Update hostname
* Fix starting/stopping messages
* Cosmetic fixes
* Remove unused script
* Revert changes, wrong root skeleton directory
* Fix backup bootcmd name
* Build usb gadget drivers (otg, g_file_storage, g_ether)
* Add driver for mxc touchscreen
* Add proper pointercal parameter for WVGA touch connected directly to MX53
* Add correct display timings for MX28LCD
* Touch device is mxc_ts for now
* Load mxc_ts on boot
* Compile ehci-hcd and mxc-otg as built-in, this makes g_file_storage work
* Update tslib version
* Tune calibration parameters
* Drop support for SMSC911X and usb network adapters
* Remove restore-rs4xx init script
* Use ifdown -f, otherwise it wont work the first time with ifplugd
* Remove obsolete script (one installed by ifplugd package will be used)
* Remove obsolete script
* Adding USB line printer support and TAP/TUN device fix to VC branch
* Merge 3964:3966 from xa2 (disable local HW echo)
* Remove CAN from kernel (will be supported by external modules)
* Remove .empty
* Add VC iomux changes, some are still commented out so that kernel can still be run on XA
* Update makepatch.sh
* Configure required iomux-pins in uboot to support 16-bit NAND
* CONFIG_CAN_FLEXCAN=m needed in external can makefile now
* Update comment
* Merge 3959:3963 and 3959:3963 from XA (add compatibilty with newer compilers)
* Merge r3980 from trunk (workaround for strstr bug)
* Remove dbus and expat from bs
* Remove oprofile and some documentation from backupsystem
* Add Spansion NAND support to kernel and uboot
* Fix factory reset support with other than ccpilot-xa platforms
* Use XA-like nand-partition layout in ARD
* Add required device nodes, fstab settings and /opt generation for ubifs
* Add backupsystem device nodes and fstab settings for ubifs
* Add opt partition attach parameters in kernel commandline, also and NAND boot parameters in ARD uboot
* Build ethernet driver for ARD as a module
* Merge 4027 from xa (add pcf2129 driver)
* Build pcf2129 driver as a module
* Add MT29F2G08 NAND to table_7 too so that it will be recognized
* Bring back front-mcu module
* Add mc34708/9 initiliazation for vc as a separate file
* Call pmic initialization from ccpilot init
* Remove wpa_supplicant
* Merge feature-checking fixes from XA
* Remove front-mcu driver again
* Add flexcan suspend code, untested
* Remove frontmcu reference
* Do not prepend mbr to uboot
* Add can/video suspend/resume code in linux initialization
* Add tvp5150 power control
* Increase size of kernel partitions from 3 to 5mb
* Remove powermonitor
* Remove powermonitor from main makefile
* Remove pwr-io driver
* Update kernel mfg defconfig
* Partition layout updated
* Enable SPI_RDY# signal in ECSPI
* Cleanup unused stuff from SS driver (including SS_SPI_HOLD, which is now handled as a built-in signal in ECSPI)
* Set tmpfs size 170M in mfg initramfs
* powermonitor no longer exists
* Fix ubi1 major number
* Adopting main makefile to VC platform style
* Added PLATFORM_VC.
* Fixed booting issue: Use ubiformat instead of flash_erase.
* Enabled VC pin I/O's. Disabled unused rails.
* Enabled VTFT, USB, Ethernet, CAN, Video.
* Fixed u-boot erase size. Renamed XA->VC. Fixed kernel command line for RGB display.
* Added new functionalities for VC. Still missing some.
* Fixes to MP_WAKEUP, PWM output, USB_EN, CAN_EN, MP_RESTART.
* Fixing update script for VC
* Use pcf2129 RTC.
* Build pcf2129 as built-in.
* Don't modprobe for pcf2129 because it is built-in.
* Added RTC, LOW power on video ports, fixes.
* Removed common-io and dig-io since they do not compile due to missing definition.
* Point /dev/rtc -> /dev/rtc0.
* Added USB storage nodes.
* Added SPIDRV to PMIC initialisation. Enabling SPI_RDY signal.
* Increased CONFIG_SYS_CBSIZE to allow longer bootargs in command line and this fixes the corrupted environment issue after saveenv. Increased CONFIG_SYS_MAXARGS to allow saving the initial bootargs with modification.
* Set PUMS[1:5] in PMIC. Set REGU_FAULT_S in PMIC according to ERRATA #2. Changed a bit SPI_RDY bits: enable SPI TX when signal is low, enable ESPI, enable HT mode.
* Fixed LOW levels to display pins.
* Device node creations added and device node links for configurable inputs and pwmout added
* Fixed platform defines for VC
* Change supply modes in PMIC.
* Modifications for VC: Use only UART3_EN port, support for read() and write() operations.
* PMIC regulator initialisation rewritten with voltages according to the specs and Linux regulator API.
* Fixing PWMOut driver
* VC: wrapper script for executing ts_calibrate via StartupLauncherGui.
* Added some device node defitions. Implemented interrupt based spi_rdy - signal handling. No more polling.
* spi_rdy - configured as gpio
* Fixed regulators init. Added 8 GPIO buttons.
* Made sure that driver wont try to enable already enabled interrupt since its enabled by default
* VC: copy pointercal defaults to /usr/local/etc instead of symlinking, so we can change the values with ts_calibrate
* Removed redundant power_off callback.
* fixed bug in CfgIn driver

