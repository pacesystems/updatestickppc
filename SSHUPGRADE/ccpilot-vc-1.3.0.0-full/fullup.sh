#!/bin/sh
#
# maximatecc - Device updater
#
# Help usage info
show_help()
{
if [ ! "x$1" == "x" ] ; then 
    echo -e "\nErr:"$1"\n" ; 
    echo -e " Run with -h for help and details.\n"
    exit 4
fi
cat <<EOF
 Reflash maximatecc device with new firmware.

Usage: `basename $0` <-p|-s> [-b file] [-k file] [-r file]

  Choice of update target:
   -p   Update Primary side images (default)
   -s   Update Backup-System images

  Default names used unless overwritten with command line:
   -b   Bootloader:  ccpilot-vc_u-boot.bin OR ccpilot-va_u-boot.bin (system detected automatically)
   -k   Kernel:      ccpilot-vc_kernel.bin OR ccpilot-va_kernel.bin
   -r   Rootfs:      ccpilot-vc_rootfs-ubi.bin OR ccpilot-va_rootfs-ubi.bin

   -f   Force-flag disables all user interaction, use for scripting.
   -t   Force updating the active side - DANGEROUS
   -m   Disable MD5 sum matching - DANGEROUS
   -u   Clear /usr/local/*
   -2   Do reboot-rescue instead of reboot after flashing 

   -v   Verbose messages
   -q   Quiet messages

 NOTE: Actions of this tool can be destructive. Use with care.
       Shutdown all excess applications BEFORE executing this update.
       Stop all file-system access and prevent preliminary power loss.

EOF
 
 exit 4
}

## Return member from array (busybox shell does not handle real array variables)
## 1 being the first item.
# args: <id>, <array>
get( ) {
 RV= 
 if [ $1 -eq 1 ] ; then RV=$2; fi
 if [ $1 -eq 2 ] ; then RV=$3; fi
 if [ $1 -eq 3 ] ; then RV=$4; fi
 if [ $1 -eq 4 ] ; then RV=$5; fi
 if [ $1 -eq 5 ] ; then RV=$6; fi
 if [ $1 -eq 6 ] ; then RV=$7; fi
 if [ ${#RV} -eq 0 ] ; then echo "Internal error with array $*"; exit 1 ; fi
 return ${#RV}
}

## Conditional Leveled echos, level 1=always, 2=normally, 3=in verbose
# args: <level> <what to echo>
decho (){
 EL=$1
 shift
 if [ $EL -lt $DL ] ; then
       /tmp/busybox echo "$*"
 fi
}
dechon (){
 EL=$1
 shift
 if [ $EL -lt $DL ] ; then
     /tmp/busybox echo -n "$*"
 fi
}
dechoe (){
 EL=$1
 shift
 if [ $EL -lt $DL ] ; then
     /tmp/busybox echo -e "$*"
 fi
}
dechoen (){
 EL=$1
 shift
 if [ $EL -lt $DL ] ; then
     /tmp/busybox echo -en "$*"
 fi
}

checkFile()
{
 FN=$1
 if [ $update_side -eq 2 ] ; then
     get $USING $BMD5
 else
     get $USING $MD5
 fi
 MD=$RV
 if [ ! -f "$MD" ] ; then
  if [ $md5 -eq 0 ] ; then 
   decho 3 " WARNING: MD5 file: Not found but Ignoring with \"-m\"" 
   return 0
  else 
   decho 1 "   ERROR: MD5 file: $MD: Not found. - Quiting.."
   exit 5
  fi
 fi
 dechon 2  "      Checking MD5: $FN: "
 
 (grep -E " $FN$" $MD || echo "" ) | md5sum -c - -s 

 if [ $? -ne 0 ] ; then 
  # error
  if [ $md5 -eq 0 ] ; then 
   decho 2 "FAILED! - Ignoring with \"-m\"" 
  else 
   decho 2 "FAILED! - Quiting.." 
   exit 5;
  fi 
 else
  decho 2 "OK"
 fi
}

get_cpu() {
 # Get our place
 id=1
 while  [ $id -le ${SUPCNT} ]  ; do
  get $id $SUPPORTCPU
  KEY=${RV}  
  grep -q $KEY /proc/cpuinfo
  if [ $? -eq 0 ] ; then
     get $id $SUPPORTED
     DEVICE=$RV
     USING=$id
     return 0
  fi
  id=$((id+1))
 done
 return 1
}

check_environment() {

 get_cpu
 if [ $? -ne 0 ] ; then
 show_help "This works only on supported devices: $SUPPORTED"; fi

 echo "   System Hardware: $DEVICE" 

 if [ `id -u` -ne 0 ] ; then show_help "This works ONLY when used as root -user!" ; fi

 ROOTUBIDEV=`grep :rootfs /proc/mounts | grep -o ^ubi.`

 this_side=0 #unknown
 if [ "$ROOTUBIDEV" == "ubi0" ] ; 
 then this_side=1; # normal
     if [ $update_side -eq 0 ] ;
	 then update_side=2; 
     fi
 else
     ROOTUBIDEV=`grep :backuprootfs /proc/mounts | grep -o ^ubi.`
     if [ "$ROOTUBIDEV" == "ubi0" ] ; then this_side=2; # backup
     if [ $update_side -eq 0 ] ;
	 then update_side=1; 
     fi
     else
	 echo "Unknown run environment... - Quiting.."
	 exit 9
     fi
 fi

 # Where are we - SSa removed the below check the files don't even fit /tmp ..
 # PWD=`pwd`
 # if [ ! ${PWD:0:4} == "/tmp" ] ; then echo "WARNING: Executing outside of /tmp, expecting problems..." ; fi
 # Read only local
 touch temporary_file_of_no_purpose 2>/dev/null
 if [ $? -ne 0 ] ; then show_help "Unable to write to local folder. Quiting.." ; 
 else
   rm -f temporary_file_of_no_purpose
 fi
}

set_defaults() {

# Supported Environments idents 
SUPPORTED="mx53-eval-sabre mx53-eval-qsb ccpilot-vc ccpilot-va"
SUPPORTCPU="Freescale.MX53.ARD.Board Freescale.MX53.LOCO.Board CCpilot.VC.MX53.Board CCpilot.VA.MX53.Board"
SUPCNT=4
DEVICE=
USING=
# Default image names
MD5="ccpilot-xa.md5sum ccpilot-xa.md5sum ccpilot-vc.md5sum ccpilot-va.md5sum"
BMD5="skip skip ccpilot-vc-bs.md5sum ccpilot-va-bs.md5sum"

BIMG=
BIMG1="ccpilot-xa_u-boot.bin ccpilot-xa_u-boot.bin ccpilot-vc_u-boot.bin ccpilot-va_u-boot.bin"
KIMG=
RIMG=
KIMG1="ccpilot-xa_kernel.bin ccpilot-xa_kernel.bin ccpilot-vc_kernel.bin ccpilot-va_kernel.bin"
RIMG1="ccpilot-xa_rootfs.bin ccpilot-xa_rootfs.bin ccpilot-vc_rootfs-ubi.bin ccpilot-va_rootfs-ubi.bin"
KIMG2="skip skip ccpilot-vc-bs_kernel.bin ccpilot-va-bs_kernel.bin"
RIMG2="skip skip ccpilot-vc-bs_rootfs-ubi.bin ccpilot-va-bs_rootfs-ubi.bin"

# Device names
BDEV=mtd0
KDEV=
RDEV=
KDEV1="mtd0 mtd1 mtd3 mtd3"
RDEV1="mtd1 mtd2 mtd4 mtd4"

KDEV2=mtd1
RDEV2=mtd2

bi=1
ki=1
ri=1

usrclear=0

ask=1
md5=1
update_side=1  # default to main system
update_this=0  # allow it quietly always (?)

DL=3 # Debug/verbose level

}

# Return 1 if argument exists
# Return 0 if argument not exist
get_argument() {
 A=$1
 RV=0
 shift
 CB=`getopt -uq -- $A "$@"`
 CB=`echo $CB| sed "s/--.*//"`
 if [ ! -z "$CB" ] ; then RV=1 ; fi
}

# Return value of argument
# Return 0 if no/empty
get_argvalue() {
 A=$1
 shift
 #echo ";$@;"
 CB=`getopt -uq -- $A: "$@" | sed "s/^ *[^ ]* \(.*\) -.*/\1/" | sed "s/\-.*//"`
 if [ -z "$CB" ] ; then  
   RV=0
 fi
 RV=$CB
}

# Return filename argument value and check for existence.
# Quit on not available
get_filearg() {
 A=$1
 shift
 #echo ";$@;"
 CB=`getopt -uq -- $A: "$@" | sed "s/^ *[^ ]* \(.*\) -.*/\1/" | sed "s/\-\-.*//"`
 if [ -z "$CB" ] ; then  
   show_help "Option -$A requires a file name argument."
 fi
 if [ ! -f "$CB" ] ; then
   show_help "File for -$A ($CB) not found!"
 fi
 RV=$CB
}

# Return filename argument valu and check for existence.
# If argument not avail, ignore.
get_optfilearg() {
 get_argument $@
 if [ $RV -eq 1 ] ; then get_filearg $@ ; else RV="" ; fi
}

# Read all arguments and set variables accordingly
read_arguments() { 

 if [ $# -gt 0 ]
 then

  get_argument q $@
  if [ $RV -eq 1 ] ; then DL=2; fi
  get_argument v $@
  if [ $RV -eq 1 ] ; then DL=4; fi

  # H elp
  get_argument h $@
  if [ $RV -eq 1 ] ; then show_help; fi

  # F orce
  get_argument f $@
  if [ $RV -eq 1 ] ; then ask=0; fi

  # M d5 check
  get_argument m $@
  if [ $RV -eq 1 ] ; then md5=0; fi

  # U sr/local delete check
  get_argument u $@
  if [ $RV -eq 1 ] ; then 
    usrclear=1; 
    de=0 # no more need for this
  fi

  # Make next reboot -> reboot-rescue
  get_argument 2 $@
  if [ $RV -eq 1 ] ; then 
    /usr/sbin/reboot-rescue.sh rescue
  fi

  # Make next reboot-rescue -> reboot
  get_argument 1 $@
  if [ $RV -eq 1 ] ; then 
    /usr/sbin/reboot-rescue.sh rescue
  fi

  get_argument s $@
  if [ $RV -eq 1 ] ; then 
    update_side=2; 
    bi=1
    ki=1
    ri=1
  fi

  get_argument p $@
  if [ $RV -eq 1 ] ; then 
    update_side=1;
    bi=1
    ki=1
    ri=1
  fi

  get_argument t $@
  if [ $RV -eq 1 ] ; then update_this=1; fi

  # Check for overrided filenames
  get_optfilearg b $@
  if [ ! -z "$RV" ] ; then BIMG="$RV" ; fi
  get_optfilearg k $@
  if [ ! -z "$RV" ] ; then KIMG="$RV" ; fi
  get_optfilearg r $@
  if [ ! -z "$RV" ] ; then RIMG="$RV" ; fi

 fi # params exist
} #readarguments

# Display info if..
show_bootloader() {
 if [ $bi -eq 1 ] ; then
  if [ -f "$BIMG" ] ; then
   decho 1 "  Requested action: Bootloader update"
   decho 3 "   Bootloader-image: $BIMG"
  else
   # no Bootloader file, think its ok.
   bi=0
  fi
 fi
}

# Display preliminary info of what we are about to do
show_request() {

# sides ok?
 if [ $this_side -eq 1 ] ; then
  decho 1 " Now running on   : normal side"
 else
  decho 1 " Now running on   : BACKUP system"
 fi
 
 if [ $update_side -eq 2 ] ; then
  if [ -z $KIMG ] ; then 
    get $USING $KIMG2; 
    KIMG=$RV;
  fi
  if [ -z $RIMG ] ; then 
    get $USING $RIMG2; 
    RIMG=$RV
  fi

  KDEV=$KDEV2  
  RDEV=$RDEV2
  show_bootloader
  dechoe 1 "  Requested action: BACKUP system update"
  dechoe 3 "      kernel-image: $KIMG"
  dechoe 3 "        root-image: $RIMG"

  if [ $this_side -eq $update_side ] ; then
   if [ $update_this -eq 1 ] ; then
     decho 3 "Active side update: BY FORCE"
   else
     echo "Active side update: Denied.";
     exit 7
   fi
  fi

 elif [ $update_side -eq 1 ] ; then
  if [ -z $BIMG ] ; then 
    get $USING $BIMG1 
    BIMG=$RV
  fi
  if [ -z $KIMG ] ; then 
    get $USING $KIMG1 
    KIMG=$RV
  fi
  if [ -z $RIMG ] ; then 
    get $USING $RIMG1
    RIMG=$RV
  fi
  get $USING $KDEV1
  KDEV=$RV
  get $USING $RDEV1
  RDEV=$RV
  show_bootloader
  dechoe 1 "  Requested action: NORMAL system update"
  dechoe 3 "      kernel-image: $KIMG"
  dechoe 3 "        root-image: $RIMG"

  if [ $this_side -ne 2 ] ; then
   if [ $update_this -eq 1 ] ; then
     decho 3 "Active side update: BY FORCE"
   else
     echo "Active side update: Denied."
     echo "                    Use reboot-rescue.sh to boot into rescue system for main system update";
     exit 7
   fi
  fi
   
#  else
#  show_help "Update side not selected. Exit."
#  exit 8
fi
}

# Acquire sizes of partitions.
get_sizes() {
 if [ ! -z "$BDEV" ] ; then
  SIZEPB=`grep $BDEV /proc/mtd | sed -r "s/.* ([0-9a-f]*) ([0-9a-f]*) \"(.*)\"/0x\1/"`
 fi

 if [ ! -z "$KDEV" ] ; then
  SIZEPK=`grep $KDEV /proc/mtd | sed -r "s/.* ([0-9a-f]*) ([0-9a-f]*) \"(.*)\"/0x\1/"`
 fi

 if [ ! -z "$RDEV" ] ; then
  SIZEPR=`grep $RDEV /proc/mtd | sed -r "s/.* ([0-9a-f]*) ([0-9a-f]*) \"(.*)\"/0x\1/"`
 fi
}

# Verify file size range
# args: <Filename> <minsize> <maxsize>
check_file_size() {
 FN=$1
 MIN=$2
 MAX=$3
 SIZEI=`ls -l $FN | tr -s \  | cut -d\  -f5`
 if [ $((SIZEI)) -lt $((MIN)) ]
  then
   echo " ERROR: Image file: $FN: Smaller than expected. - Quiting.."
   exit 1
  fi
  if [ $((SIZEI)) -gt $((MAX)) ]
  then
   echo " ERROR: Image file: $FN: Larger than expected. - Quiting.."
   exit 1
  fi
}

# Verify image correctness
check_bootstrap_image() {
 if [ ! -f "$BIMG" ] ; then
  echo " ERROR: Image file: $BIMG: Not found. Quiting.."
  bi=0
  exit 1
 else 
  checkFile $BIMG
  check_file_size $BIMG 5000 8000
 fi 
}

check_bootloader_image() {
    echo "check_bootloader_image"
 if [ ! -f "$BIMG" ] ; then
  echo " ERROR: Image file: $BIMG: Not found. Quiting.."
  bi=0
  exit 1
 else 
  checkFile $BIMG
  check_file_size $BIMG 10000 $SIZEPB
 fi 

}

# Verify image correctness
check_kernel_image() {
 if [ ! -f "$KIMG" ] ; then
   ki=0
   if [ $bi -eq 1 ] ; then
     # allow bootstrap only update
     echo " WARNING:    Image: $KIMG: Not found. - Skipping.."
   else
     echo " ERROR: Image file: $KIMG: Not found. - Quiting.."
     exit 1
   fi
 else 
  checkFile $KIMG

  check_file_size $KIMG 120000 $SIZEPK
 fi 
}

# Verify image correctness
check_root_image() {
 if [ ! -f "$RIMG" ] ; then
   ri=0
   if [ $bi -eq 1 ] ; then
     # allow bootsrap only update
     echo " WARNING:    Image: $RIMG: Not found. - Skipping.."
   else
     echo " ERROR: Image file: $RIMG: Not found. - Quiting.." 
     exit 1
   fi
 else 
  checkFile $RIMG
  # known sizes 091106 bs-root: 9699328, main-root: 21102592
  check_file_size $RIMG 9000000 $SIZEPR
 fi 
}

# Verify files, iff
check_files() {
 get_sizes

 if [ $bi -eq 1 ] ; then
  check_bootloader_image
 fi
 if [ $ki -eq 1 ] ; then
   check_kernel_image
 fi
 if [ $ri -eq 1 ] ; then
   check_root_image
 fi
}

# Args: <filename> <source> <test-cmd>
copyhere ()
{
 if [ ! -x $1 ] ; then
  cp -f $2 .
 fi
 if [ ! -x $1 ] ; then
  echo "Error: Local version of '$1' missing. - Quiting.."
  exit 1
 fi
 ./$3 >/dev/null
 if [ $? -ne 0 ] ; then
  echo "Error: Nonworking version of local '$1'. - Quiting.."
  exit 2
 fi
}

copytmp ()
{
 if [ ! -x $1 ] ; then
  cp -f $2 /tmp
 fi
 #if [ ! -x $1 ] ; then
 # echo "Error: Local version of '$1' missing. - Quiting.."
 # exit 1
 #fi
 /tmp/$3 >/dev/null
 if [ $? -ne 0 ] ; then
  echo "Error: Nonworking version of local '$1'. - Quiting.."
  exit 2
 fi
}

# Init-like
prepare_environment() {
 prepare_services

 rm -f /tmp/mtd*
 mknod /tmp/mtd0 c 90 0 && 
 mknod /tmp/mtd1 c 90 2 && 
 mknod /tmp/mtd2 c 90 4 &&
 mknod /tmp/mtd3 c 90 6 &&
 mknod /tmp/mtd4 c 90 8 &&
 mknod /tmp/mtd5 c 90 10 &&
 mknod /tmp/mtd6 c 90 12 &&
 mknod /tmp/mtd7 c 90 14 &&
 mknod /tmp/mtd8 c 90 16 || show_help "Prepare failed"
 sleep 1
}

# Check for required tools
prepare_tools(){
 copytmp busybox /bin/busybox "busybox echo 'good'"
 copytmp flash_erase /usr/sbin/flash_erase "flash_erase --version"
 copytmp kobs-ng /usr/bin/kobs-ng "busybox echo 'good'"
 copytmp nandwrite /usr/sbin/nandwrite "nandwrite --version"
}

# Check constrains for actions
check_limits(){
 if [ `expr $ki + $ri` -eq 1 ]
 then
  echo -e "WARNING: Kernel and Rootfile images are needed together."
  ki=0
  ri=0
 fi
 if [ `expr $bi + $ki + $ri` -eq 0 ]
 then
  echo "ERROR: Cant flash without accepted set of images. - Quiting.."
  exit 1
 fi
}

# Listing parts of actions to do 
show_effects() {
 echo
 echo  "       === Listing: Actions: ==="
 if [ $bi -eq 1 ]
 then
  decho 1 "          Updating: bootloader  "
 fi
 if [ $usrclear -eq 1 ]
 then
  decho 1 "          Clearing: /usr/local  ALL FILES WILL BE LOST."
 fi
 if [ $ki -eq 1 ]
 then
  decho 1 "          Updating: kernel"
 fi
 if [ $ri -eq 1 ]
 then
  decho 1   "          Updating: root-fs      ALL FILES ON ROOT-FS WILL BE LOST"
  if [ $usrclear -eq 1 ] ; then
    decho 2 "                    Files on /media/cf unaffected" 
  else
    decho 2 "                    Files on /usr/local and /media/cf unaffected" 
  fi
 fi
}

prepare_services(){
 decho 3 "         Preparing: just a minute.."
 if [ -e /etc/init.d/logrotate ] ; then
 /etc/init.d/logrotate stop 2>&1 >/dev/null; fi
 # protect from scarey RW mode
 mount -oremount,ro /
 #/etc/rcS.d/S90powerd stop
 sleep 1
}

# Final Confirmation
confirm_user(){
 if [ $ask -eq 1 ] ; then
  echo -e "\n  === Verification: ARE YOU SURE ? ===\n"
  echo -e   "    - If yes, press Enter to continue."
  echo -e   "    - IF NOT, press CTRL+C now to cancel update."
  decho 2   "   This is you last chance to do cancel!"
  dechoe 1  "\n** IF YOU CONTINUE, DO NOT INTERRUPT THE PROCESS UNTIL READY **" 
  read a
 else   
  echo -e "\n      Verification: Skipped, continuing.."
 fi
 dechoe 2 "\n     === Now doing: Requested actions ==="

 dechoe 1 "\n * DO NOT BREAK PROCESS OR SHUTDOWN THE UNIT before update is complete. *\n"
 sleep 1
}

# Usr local clear
clear_usrlocal() {
 /tmp/busybox echo "Cleaning /usr/local .."
  rm -rf /usr/local/*
}

##
## FLASH EM
##
flash_bootloader() {
 /tmp/busybox echo "Erasing old bootloader.."
 /tmp/flash_erase /tmp/$BDEV 0 0
 /tmp/busybox echo "Copying new bootloader image.."
 echo 1 > /sys/devices/platform/mxc_nandv2_flash.0/disable_bi_swap
 /tmp/kobs-ng init --chip_0_device_path=/tmp/$BDEV $BIMG
 echo 0 > /sys/devices/platform/mxc_nandv2_flash.0/disable_bi_swap
}

flash_kernel() {
 /tmp/busybox echo "Erasing old kernel.."
 /tmp/flash_erase /tmp/$KDEV 0 0
 /tmp/busybox echo "Copying new kernel image.."
 /tmp/nandwrite -p /tmp/$KDEV $KIMG
}

flash_root() {
 /tmp/busybox echo "Erasing old root.."
 /tmp/flash_erase /tmp/$RDEV 0 0
 /tmp/busybox echo "Copying new root-fs image.."
 /tmp/nandwrite /tmp/$RDEV $RIMG
}

do_reboot(){
 # All done, rebooooot
 /tmp/busybox echo "Completed. Rebooting.."
 /tmp/busybox reboot
}

# YEY... main()

echo  "===== maximatecc: Device Update ===== "$V

set_defaults

read_arguments $@

check_environment 

prepare_tools

show_request

check_files

check_limits

prepare_environment

show_effects

confirm_user

if [ $usrclear -eq 1 ] ; then
 clear_usrlocal
fi

if [ $bi -eq 1 ] ; then
 flash_bootloader
fi

if [ $ki -eq 1 ] ; then
 flash_kernel
fi

if [ $ri -eq 1 ] ; then
 flash_root
fi

do_reboot

/tmp/busybox echo -e "\nEnd of all actions, H o l d    o n    t i g h t \n"
