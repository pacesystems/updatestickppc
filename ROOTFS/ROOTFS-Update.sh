#!/bin/sh

# Variables Start
MACHINEARCH=$(uname -m)
DESTDIR="/opt/bin"
BACKUPDIR="/media/usb/Backup"
if [ -c /dev/serial-eeprom ]; then          # Determine if /dev/serial-eeprom is a character oriented device so we can reas serial from it
    SERIAL=$(sed -n '/^UNIT_SERIAL:/{s/UNIT_SERIAL://;p}' /dev/serial-eeprom)
    SERIAL=$(echo $(echo $SERIAL | sed 's/[^0-9]*//g'))
else                                        # Otherwise Serial is one
    SERIAL=1
fi
# Variables End

# Functions Start
log() {
    if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
        echo -e "$*" >> /dev/tty0
    else
        echo -e "$*"
    fi
}

printstart() {
    log "Start"
    log ""
    log ""
    sleep 1s
}

printupdate() {
    log "*******************************************************************"
    log "*                                                                 *"
    log "*      ██╗   ██╗██████╗ ██████╗  █████╗ ████████╗███████╗         *"
    log "*      ██║   ██║██╔══██╗██╔══██╗██╔══██╗╚══██╔══╝██╔════╝         *"
    log "*      ██║   ██║██████╔╝██║  ██║███████║   ██║   █████╗           *"
    log "*      ██║   ██║██╔═══╝ ██║  ██║██╔══██║   ██║   ██╔══╝           *"
    log "*      ╚██████╔╝██║     ██████╔╝██║  ██║   ██║   ███████╗         *"
    log "*       ╚═════╝ ╚═╝     ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝         *"
    log "*                                                                 *"
    log "*******************************************************************"
}

printok() {
    log "********************************"
    log "*                              *"
    log "*      ██████╗ ██╗  ██╗        *"
    log "*     ██╔═══██╗██║ ██╔╝        *"
    log "*     ██║   ██║█████╔╝         *"
    log "*     ██║   ██║██╔═██╗         *"
    log "*     ╚██████╔╝██║  ██╗        *"
    log "*      ╚═════╝ ╚═╝  ╚═╝        *"
    log "*                              *"
    log "********************************"
}

printerror() {
    log "***********************************************"
    log "*                                             *"
    log "*  ███████╗██████╗ ██████╗  ██████╗ ██████╗   *"
    log "*  ██╔════╝██╔══██╗██╔══██╗██╔═══██╗██╔══██╗  *"
    log "*  █████╗  ██████╔╝██████╔╝██║   ██║██████╔╝  *"
    log "*  ██╔══╝  ██╔══██╗██╔══██╗██║   ██║██╔══██╗  *"
    log "*  ███████╗██║  ██║██║  ██║╚██████╔╝██║  ██║  *"
    log "*  ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝  *"
    log "*                                             *"
    log "***********************************************"
}
# Functions End

if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
    log "# \033[92mUpdating or installing Application-Basefiles on PPC:\033[0m"
    log "#"
    log "# $PWD"
    if [ -d opt ]; then
        log "Copying rootfs in place!"
        cp -r opt/* /opt
    else
        log "\033[91mNo rootfs for Update or insitial instalation found!\033[0m"
        sleep 10s
    fi
    if [ -e secret.cfg ]; then
        log "\033[93mFound secret.cfg. Updating it!\033[0m"
        cp secret.cfg $DESTDIR/cfg/secret.cfg
    else
        log "\033[91mNo secret.cfg for installation found!\033[0m"
        sleep 10s
    fi
else
    log "\033[91m# NOT Updating rootfs of PPC\033[0m"
    log "#"
    sleep 10s
fi

exit 0
