#!/bin/sh

# Source the config file that holds CFG_DIR variable
. /opt/etc/pace-start.config

if [ -x "$CFG_DIR/runAppl.sh" ]; then
  $CFG_DIR/runAppl.sh "$@"
fi
