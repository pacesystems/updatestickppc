#!/bin/sh

# Microcontroller CAN settings
MC_CAN_0="can0"
MC_BITRATE=500000
MC_BUSOFF_RECOVERY=250
MC_TXQUEUELEN=4000

# Application settings
APPLICATION_PATH=`dirname $0`
APPLICATION="AFA2GenPlus"

load_bcm_module() {
  lsmod | grep -q "can_bcm"

  if [ $? -ne 0 ]; then
    insmod "/lib/modules/2.6.35.3/kernel/drivers/cc-drivers/can-bcm.ko"
  fi
  lsmod | grep -q "can_isotp"

  if [ $? -ne 0 ]; then
    insmod "/lib/modules/2.6.35.3/kernel/drivers/cc-drivers/can-isotp.ko"
  fi
}

setup_can_mc() {
  # Microcontroller CAN
  ifconfig $MC_CAN_0 down
  echo $MC_BITRATE > "/sys/class/net/${MC_CAN_0}/can_bittiming/bitrate"
  echo $MC_BUSOFF_RECOVERY > "/sys/class/net/${MC_CAN_0}/can_restart_ms"
  ifconfig $MC_CAN_0 txqueuelen $MC_TXQUEUELEN
  ifconfig $MC_CAN_0 up
}

# Application startup
case "$1" in
  start)
    # Disable Framebuffer Blanking
    echo -e '\033[9;0]' > /dev/tty1
    load_bcm_module
    setup_can_mc
      
    # Start Application
	  ( \
	  QT_QPA_EGLFS_DISABLE_INPUT=1 \
	  TSLIB_TSDEVICE=/dev/touch0 \
	  QT_QPA_EGLFS_HIDECURSOR=1 \
	  LD_LIBRARY_PATH=$APPLICATION_PATH \
	  $APPLICATION_PATH/$APPLICATION -platform eglfs -plugin tslib -plugin evdevkeyboard:/dev/input/event0 >> $APPLICATION_PATH/$APPLICATION.log 2>&1 &
	  )
    ;;

  stop)
    killall $APPLICATION
    ;;

  *)
    echo "usage: $0 {start|stop}"
    exit 1
    ;;

esac

exit 0
