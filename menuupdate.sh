#!/bin/sh

# Variables Start
USBDIR=/media/usb
MACHINEARCH=$(uname -m)
UPDATED=0
MENUSTRING=""
rows=$(stty size | awk '{print $1}')
cols=$(stty size | awk '{print $2}')
if [ "$MACHINEARCH" != "armv7l" ]; then
    rows=$(expr $rows - 1)
else
    rows=29
    cols=100
fi

prows=$(expr $rows - 2 )
hrows1=$(expr $prows / 2 )
hrows2=$(expr $hrows1 / 2 )
hrows3=$(expr $hrows2 / 2 )
partrows=$(expr \( \( $rows - 2 \) \* 8 \) / 64 )
row1=$(expr 1 + 1 + $hrows3 )
row2=$(expr 1 + 1 + $hrows3 + $hrows2)
row3=$(expr $rows - 1 - $hrows3  - $hrows2 )
row4=$(expr $rows - 1 - $hrows3 )

ii=1
seline=""
while [ "$ii" -le "$cols" ]; do
    seline="$seline*"
    ii=$(expr $ii + 1)
done

ii=1
rline=""
while [ "$ii" -le "$cols" ]; do
    if [ "$ii" -eq "1" -o "$ii" -eq "$cols" ]; then 
        rline="$rline*"
    else
        rline="$rline "
    fi
    ii=$(expr $ii + 1)
done


Str1="( ) Update MCU"
LStr1=${#Str1}
Str2="( ) Update System"
LStr2=${#Str2}
Str3="( ) Update Database"
LStr3=${#Str3}
Str4="( ) Update Serial-Key"
LStr4=${#Str4}
Str5="Update Application ( )"
LStr5=${#Str5}
Str6="Update RootFS ( )"
LStr6=${#Str6}
Str7="F7 ( )"
LStr7=${#Str7}
Str8="Exit ( )"
LStr8=${#Str8}

if [ "$MACHINEARCH" = "armv7l" ]; then
    device='/dev/input/event0'
    key='*(Key)*'
    key_f1='*(F1), value 1*'
    key_f2='*(F2), value 1*'
    key_f3='*(F3), value 1*'
    key_f4='*(F4), value 1*'
    key_f5='*(F5), value 1*'
    key_f6='*(F6), value 1*'
    key_f7='*(F7), value 1*'
    key_f8='*(F8), value 1*'
    key_pwr='*(Power), value 1*'
else
    device='/dev/input/event3'
    key='*(EV_KEY)*'
    key_f1='*(KEY_F1), value 1*'
    key_f2='*(KEY_F2), value 1*'
    key_f3='*(KEY_F3), value 1*'
    key_f4='*(KEY_F4), value 1*'
    key_f5='*(KEY_F5), value 1*'
    key_f6='*(KEY_F6), value 1*'
    key_f7='*(KEY_F7), value 1*'
    key_f8='*(KEY_F8), value 1*'
    key_pwr='*(Power), value 1*'
fi
# Variables End

# Functions Start
log() {
    if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
        echo -e "$*" >> /dev/tty0
    else
        echo -e "$*"
    fi
}

cleartty() {
    clear >> /dev/tty0
}

stopapplication() {
    if [ -x /opt/bin/pace-start.sh ]; then  # Stop our Application
            /opt/bin/pace-start.sh stop
    else                                    # Stop default Application from CC
            killall StartupLauncherGui
    fi
}

restartapplication() {
    log "# restarting application"
    log "#"
    if [ -x /opt/bin/pace-start.sh ]; then  # Start our Application
            /opt/bin/pace-start.sh start
    else                                    # Start default Application from CC
            StartupLauncherGui
    fi
}

updatemcu() {
    if [ -d MCU ] ; then
        log "# Folder MCU found"
        cd MCU
        if [ -e ./MCU-Update.sh ] ; then
            log "# MCU update script found"
            chmod +x ./MCU-Update.sh
            ./MCU-Update.sh
            if [ $? -eq 0 ]; then
                UPDATED=$(expr $UPDATED + 1)
            fi
        else
            log "# MCU update script NOT found"
            log "#"
        fi
        cd ..
    else
        log "# NO update for MCU found on USB-Stick"
        log "#"
    fi
}

updatesys() {
    if [ -d SYS ]; then
        log "# Folder SYS found"
        cd SYS
        if [ -e ./SYS-Update.sh ]; then
            log "# System update script found"
            chmod +x ./SYS-Update.sh
            ./SYS-Update.sh
            if [ $? -eq 0 ]; then
                UPDATED=$(expr $UPDATED + 1)
            fi
        else
            log "# System update script NOT found"
            log "#"
        fi
        cd ..
    else
        log "# NO update for system found on USB-Stick"
        log "#"
    fi
}

updatedb() {
    if [ -d DB ]; then
        log "# Folder DB found"
        cd DB
        if [ -e ./DB-Update.sh ]; then
            log "# Database update script found"
            chmod +x ./DB-Update.sh
            ./DB-Update.sh
            if [ $? -eq 0 ]; then
                UPDATED=$(expr $UPDATED + 1)
            fi
        else
            log "# Database update script not found"
            log "#"
        fi
        cd ..
    else
        log "# No update for database found on USB-Stick"
        log "#"
    fi
}

updateapp() {
    if [ -d PPC ]; then
        log "# Folder PPC found"
        cd PPC
        if [ -e ./PPC-Update.sh ]; then
            log "# Application update script found"
            chmod +x ./PPC-Update.sh
            ./PPC-Update.sh
            if [ $? -eq 0 ]; then
                UPDATED=$(expr $UPDATED + 1)
            fi
        else
            log "# Application update script not found"
            log "#"
        fi
        cd ..
    else
        log "# No update for application found on USB-Stick"
        log "#"
    fi
}

updatekey() {
    if [ -d KEYS ]; then
        log "# Folder KEYS found"
        cd KEYS
        if [ -e ./KEYS-Update.sh ]; then
            log "# Keys update script found!"
            chmod +x ./PPC-Update.sh
            ./KEYS-Update.sh
            if [ $? -eq 0 ]; then
                UPDATED=$(expr $UPDATED + 1)
            fi
        else
            log "# Keys update script not found"
            log "#"
        fi
        cd ..
    else
        log "# No update for serial-key found on USB-Stick"
        log "#"
    fi
}

updaterootfs() {
    if [ -d ROOTFS ]; then
        log "# Folder ROOTFS found"
        cd ROOTFS
        if [ -e ./ROOTFS-Update.sh ]; then
            log "# Rootfs update script found!"
            chmod +x ./ROOTFS-Update.sh
            ./ROOTFS-Update.sh
            if [ $? -eq 0 ]; then
                UPDATED=$(expr $UPDATED + 1)
            fi
        else
            log "# Rootfs update script not found"
            log "#"
        fi
        cd ..
    else
        log "# No update for rootfs found on USB-Stick"
        log "#"
    fi
}

buildmenu() {
    i=1
    #cleartty
    while [ "$i" -le "$rows" ]; do
        if [ "$i" -eq "1" -o "$i" -eq "$rows" ]; then
#             log ""
            #log "$seline"
            MENUSTRING="$MENUSTRING$seline"
        elif [ "$i" -eq "$row1" ]; then
            rlength=$(expr ${#rline} - $LStr1 - $LStr5)
            linex="$(echo "$rline" | awk -v LStr1=$LStr1 -v rlength=$rlength '{print substr($rline, LStr1, rlength)}')"
#             linex=${rline:$LStr1:$rlength}
            line="$Str1$linex$Str5"
            #log "$line"
            MENUSTRING="$MENUSTRING$line"
        elif [ "$i" -eq "$row2" ]; then
            rlength=$(expr ${#rline} - $LStr2 - $LStr6)
            linex="$(echo "$rline" | awk -v LStr2=$LStr2 -v rlength=$rlength '{print substr($rline, LStr2, rlength)}')"
#             linex=${rline:$LStr2:$rlength}
            line="$Str2$linex$Str6"
            #log "$line"
            MENUSTRING="$MENUSTRING$line"
        elif [ "$i" -eq "$row3" ]; then
            rlength=$(expr ${#rline} - $LStr3 - $LStr7)
            linex="$(echo "$rline" | awk -v LStr3=$LStr3 -v rlength=$rlength '{print substr($rline, LStr3, rlength)}')"
#             linex=${rline:$LStr3:$rlength}
            line="$Str3$linex$Str7"
            #log "$line"
            MENUSTRING="$MENUSTRING$line"
        elif [ "$i" -eq "$row4" ]; then
            rlength=$(expr ${#rline} - $LStr4 - $LStr8)
            linex="$(echo "$rline" | awk -v LStr4=$LStr4 -v rlength=$rlength '{print substr($rline, LStr4, rlength)}')"
#             linex=${rline:$LStr4:$rlength}
            line="$Str4$linex$Str8"
            #log "$line"
            MENUSTRING="$MENUSTRING$line"
        else
#             log ""
            #log "$rline"
            MENUSTRING="$MENUSTRING$rline"
        fi
        i=$(expr $i + 1)
    done
}

printmenu() {
    cleartty
    log "$MENUSTRING"
}

waitforentry() {
    script -c "evtest /dev/input/event0" /dev/null | while read line; do
       #log $line
        case $line in
            ($key)
                case $line in
                    $key_stop) cleartty; log "Stop Pressed";
                        break ;;
                    $key_f1)   cleartty; log "F1 Pressed"; updatemcu;
                        sleep 1s; printmenu ;;
                    $key_f2)   cleartty; log "F2 Pressed"; updatesys;
                        sleep 1s; printmenu ;;
                    $key_f3)   cleartty; log "F3 Pressed"; updatedb;
                        sleep 1s; printmenu ;;
                    $key_f4)   cleartty; log "F4 Pressed"; updatekey;
                        sleep 1s; printmenu ;;
                    $key_f5)   cleartty; log "F5 Pressed"; updateapp;
                        sleep 1s; printmenu ;;
                    $key_f6)   cleartty; log "F6 Pressed"; updaterootfs; 
                        sleep 1s; printmenu ;;
                    $key_f7)   cleartty; log "F7 Pressed";
                        sleep 1s; printmenu ;;
                    $key_f8)   cleartty; log "F8 Pressed";
                        break ;;
                    $key_pwr)  cleartty; log "Power Pressed";
                        ;;
                    *) ;; #cleartty; log "Unknown Button Pressed";
                        # sleep 1s; printmenu ;;
            esac
            ;;
        esac
    done
}

usbremove() {
   log "#"
   log "# Please Remove USB Stick"
   log "#"
   while :
   do
     if ( ! mountpoint -q /media/usb ) then
       break
     fi
     sleep 1s
   done
}

rebootdevice() {
    log "# rebooting device"
    log "#"
    reboot
}
# Functions End

if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
    top -n 1 >> $USBDIR/TOPCAT_$(date "+%F-%H-%M-%S")
    stopapplication                         # Stop running Application
    sleep 2s                                # Wait some seconds to finish
    chvt 2                                  # Change virtual Terminal
    cd $USBDIR
else
    log ""
    log "Not an ARM Machine ($MACHINEARCH is not supported)!"
    log ""
    sleep 2s
fi

buildmenu
log "$MENUSTRING"
printmenu

waitforentry

if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
    usbremove
    if [ $UPDATED -ge 1 ]; then
        rebootdevice
    else
    # in this case we can restart application instead of rebooting device
        cleartty
        chvt 1
        cleartty
        restartapplication
    fi
fi

exit 0
