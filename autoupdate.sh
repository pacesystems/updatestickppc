#!/bin/sh

# Variables Start
USBDIR=/media/usb
MACHINEARCH=$(uname -m)
UPDATED=0
# Variables End

# Functions Start
log() {
    if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
        echo -e "$*" >> /dev/tty0
    else
        echo -e "$*"
    fi
}

cleartty() {
    clear >> /dev/tty0
}

stopapplication() {
    if [ -x /opt/bin/pace-start.sh ]; then  # Stop our Application
            /opt/bin/pace-start.sh stop
    else                                    # Stop default Application from CC
            killall StartupLauncherGui
    fi
}

restartapplication() {
    log "# restarting application"
    log "#"
    if [ -x /opt/bin/pace-start.sh ]; then  # Start our Application
            /opt/bin/pace-start.sh start
    else                                    # Start default Application from CC
            StartupLauncherGui
    fi
}

printstart() {
    log "Start"
    log ""
    sleep 1s
}

printupdate() {
    log "*******************************************************************"
    log "*                                                                 *"
    log "*      ██╗   ██╗██████╗ ██████╗  █████╗ ████████╗███████╗         *"
    log "*      ██║   ██║██╔══██╗██╔══██╗██╔══██╗╚══██╔══╝██╔════╝         *"
    log "*      ██║   ██║██████╔╝██║  ██║███████║   ██║   █████╗           *"
    log "*      ██║   ██║██╔═══╝ ██║  ██║██╔══██║   ██║   ██╔══╝           *"
    log "*      ╚██████╔╝██║     ██████╔╝██║  ██║   ██║   ███████╗         *"
    log "*       ╚═════╝ ╚═╝     ╚═════╝ ╚═╝  ╚═╝   ╚═╝   ╚══════╝         *"
    log "*                                                                 *"
    log "*******************************************************************"
    log ""
}

printok() {
    log "********************************"
    log "*                              *"
    log "*      ██████╗ ██╗  ██╗        *"
    log "*     ██╔═══██╗██║ ██╔╝        *"
    log "*     ██║   ██║█████╔╝         *"
    log "*     ██║   ██║██╔═██╗         *"
    log "*     ╚██████╔╝██║  ██╗        *"
    log "*      ╚═════╝ ╚═╝  ╚═╝        *"
    log "*                              *"
    log "********************************"
}

printerror() {
    log "***********************************************"
    log "*                                             *"
    log "*  ███████╗██████╗ ██████╗  ██████╗ ██████╗   *"
    log "*  ██╔════╝██╔══██╗██╔══██╗██╔═══██╗██╔══██╗  *"
    log "*  █████╗  ██████╔╝██████╔╝██║   ██║██████╔╝  *"
    log "*  ██╔══╝  ██╔══██╗██╔══██╗██║   ██║██╔══██╗  *"
    log "*  ███████╗██║  ██║██║  ██║╚██████╔╝██║  ██║  *"
    log "*  ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝  *"
    log "*                                             *"
    log "***********************************************"
}

usbremove() {
   log "#"
   log "# Please Remove USB Stick"
   log "#"
   while :
   do
     if ( ! mountpoint -q /media/usb ) then
       break
     fi
     sleep 1s
   done
}

rebootdevice() {
    log "# rebooting device"
    log "#"
    reboot
}
# Functions End

if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
    stopapplication                         # Stop running Application
    sleep 2s                                # Wait some seconds to finish
    chvt 2                                  # Change virtual Terminal
    cd $USBDIR
else
    log ""
    log "Not an ARM Machine ($MACHINEARCH is not supported)!"
    log ""
fi

    printstart
    printupdate

    if [ -d MCU ] ; then
        log "# Folder MCU found"
        cd MCU
        if [ -e ./MCU-Update.sh ] ; then
            log "# MCU update script found"
            chmod +x ./MCU-Update.sh
            ./MCU-Update.sh
            if [ $? -eq 0 ]; then
                UPDATED=$(expr $UPDATED + 1)
            fi
        else
            log "# MCU update script NOT found"
            log "#"
        fi
        cd ..
    else
        log "# NO update for MCU found on USB-Stick"
        log "#"
    fi

    if [ -d SYS ]; then
        log "# Folder SYS found"
        cd SYS
        if [ -e ./SYS-Update.sh ]; then
            log "# System update script found"
            chmod +x ./SYS-Update.sh
            ./SYS-Update.sh
            if [ $? -eq 0 ]; then
                UPDATED=$(expr $UPDATED + 1)
            fi
        else
            log "# System update script NOT found"
            log "#"
        fi
        cd ..
    else
        log "# NO update for system found on USB-Stick"
        log "#"
    fi

    if [ -d PPC ]; then
        log "# Folder PPC found"
        cd PPC
        if [ -e ./PPC-Update.sh ]; then
            log "# Application update script found"
            chmod +x ./PPC-Update.sh
            ./PPC-Update.sh
            if [ $? -eq 0 ]; then
                UPDATED=$(expr $UPDATED + 1)
            fi
        else
            log "# Application update script not found"
            log "#"
        fi
        cd ..
    else
        log "# No update for application found on USB-Stick"
        log "#"
    fi

if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
    usbremove
    if [ $UPDATED -ge 1 ]; then
        rebootdevice
    else
    # in this case we can restart application instead of rebooting device
        cleartty
        chvt 1
        cleartty
        restartapplication
#         rebootdevice
    fi
fi

exit 0
