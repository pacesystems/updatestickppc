#!/bin/sh

# Variables Start
MACHINEARCH=$(uname -m)
TEMPDIR="/opt/bin/temp"
CFGDIR="/opt/bin/cfg"
# Variables End

# Functions Start
log() {
    if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
        echo -e "$*" >> /dev/tty0
    else
        echo -e "$*"
    fi
}

printok() {
    log "********************************"
    log "*                              *"
    log "*      ██████╗ ██╗  ██╗        *"
    log "*     ██╔═══██╗██║ ██╔╝        *"
    log "*     ██║   ██║█████╔╝         *"
    log "*     ██║   ██║██╔═██╗         *"
    log "*     ╚██████╔╝██║  ██╗        *"
    log "*      ╚═════╝ ╚═╝  ╚═╝        *"
    log "*                              *"
    log "********************************"
}

printerror() {
    log "***********************************************"
    log "*                                             *"
    log "*  ███████╗██████╗ ██████╗  ██████╗ ██████╗   *"
    log "*  ██╔════╝██╔══██╗██╔══██╗██╔═══██╗██╔══██╗  *"
    log "*  █████╗  ██████╔╝██████╔╝██║   ██║██████╔╝  *"
    log "*  ██╔══╝  ██╔══██╗██╔══██╗██║   ██║██╔══██╗  *"
    log "*  ███████╗██║  ██║██║  ██║╚██████╔╝██║  ██║  *"
    log "*  ╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚═╝  ╚═╝  *"
    log "*                                             *"
    log "***********************************************"
}
# Functions End

if [ "$MACHINEARCH" = "armv7l" ]; then      # Prevent running on other Architectures especially Dev-PC
    log "# \033[92mUpdating Firmware MCU:\033[0m"
    log "#"
    log "# Preparing Temp Dir..."
    log "#"
    [ -d $TEMPDIR ] || { mkdir $TEMPDIR;  }
    rm -rf $TEMPDIR/*
    log "# Unpack Update..."
    log "#"
    tar -xzf ./cc-auto.bin -C $TEMPDIR > /dev/null
    SCRIPT=$TEMPDIR/setup.sh
    if [ ! $? -eq 0 ]; then
        log "\033[91m*** 21 ERROR*** Error Extracting Update!\033[0m"
        rm -r $TEMPDIR/* > /dev/null
        printerror
        sleep 10s
    else
        if [ ! -e $SCRIPT ]; then
            log "\033[91m*** 20 ERROR*** Installer not found!\033[91m"
            rm -r $TEMPDIR/* > /dev/null
            printerror
            sleep 10s
        else  # Unpack Successfull
            log "# Running Updater..."
            log "#"
            chmod +x $SCRIPT
            $SCRIPT
            INSTAPPLRESULT=$?
            if [ ! $INSTAPPLRESULT -eq 0 ]; then
                printerror
            else # Installation Successfull
                if [ ! -d $CFGDIR ]; then
                    mkdir $CFGDIR
                fi
                if [ -e $TEMPDIR/bootloader.cfg ]; then
                    cp $TEMPDIR/bootloader.cfg $CFGDIR/bootloader.cfg
                fi
                if [ -e $TEMPDIR/firmware.cfg ]; then
                    cp $TEMPDIR/firmware.cfg $CFGDIR/firmware.cfg
                fi
                printok
            fi

            rm -r $TEMPDIR/* > /dev/null # Clean Tempdir after Installing
        fi
    fi
else
    log "# \033[91mNOT Updating MCU!\033[0m"
    log "#"
    sleep 10s
fi

exit 0
